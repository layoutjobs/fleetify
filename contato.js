$('#contact-form').submit(function(e) {
    e.preventDefault();
    const nome = $('input[name="nome"]').val();
    const email = $('input[name="email"]').val();
    const telefone = $('input[name="telefone"]').val();
    const msg = $('input[name="msg"]').val();
    $.ajax({
        url: 'contato.php', // caminho para o script que vai processar os dados
        type: 'POST',
        data: {nome: nome, email: email, telefone: telefone, msg: msg},
        success: function(response) {
            $('#resp').html(response);
        },
        error: function(xhr, status, error) {
            alert(xhr.responseText);
        }
    });
    return false;
});