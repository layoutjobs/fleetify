(function($) {

    'use strict';

    var hash = location.hash;

    var smoothScroll = function(hash) {
        var target = document.getElementById(hash.replace('#', ''));
        var offsetTop = target && target !== '#' ? target.offsetTop : 0;

        $('html, body').stop().animate({
            'scrollTop': offsetTop
        }, 500);

        if (history.pushState) {
            history.pushState(null, null, hash);
        } else {
            location.hash = hash;
        }
    }

    $('[data-smooth-scroll]').on('click', function(event) {
        smoothScroll(this.hash);
        event.preventDefault();
    });

    if (hash) {
        if ($(hash).is('.modal')) {
            $(hash).modal({ show: true });
        } else {
            // smoothScroll(hash);
        }
    }


    $(document).ready(function(){
    
     $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        
        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
        
        $('#back-to-top').tooltip('show');

});


})(jQuery);


/**************************************
    MAPA
*************************************/

function initialize() {

  // Exibir mapa;
  var myLatlng = new google.maps.LatLng(-23.084405, -47.176194);
  var mapOptions = {
    zoom: 17,
    center: myLatlng,
    panControl: false,
    // mapTypeId: google.maps.MapTypeId.ROADMAP
    mapTypeControlOptions: {
      mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
    }
  }


  // Parâmetros do texto que será exibido no clique;
  var contentString = '<h2>REI Brasil</h2>' +
  '<p>Rod. Engenheiro Ermenio de Oliveira Penteado - KM 57,7<br> Tombadouro Marginal Norte<br> Condominio Industriale - Galpão 7 - Tombadouro,<br> Indaiatuba - SP,<br> CEP: 13337-300</p>' +
  '<a href="https://goo.gl/maps/bGeuVRSpVaL2" target="_blank">clique aqui para mais informações</a>';
  var infowindow = new google.maps.InfoWindow({
      content: contentString,
      maxWidth: 700
  });


  // Exibir o mapa na div #mapa;
  var map = new google.maps.Map(document.getElementById("mapa"), mapOptions);


  // Marcador personalizado;
  var image = 'icon-map.png';
  var marcadorPersonalizado = new google.maps.Marker({
      position: myLatlng,
      map: map,
      icon: image,
      title: 'REI Brasil',
      animation: google.maps.Animation.DROP
  });


//   // Exibir texto ao clicar no ícone;
  google.maps.event.addListener(marcadorPersonalizado, 'click', function() {
    infowindow.open(map,marcadorPersonalizado);
  });


  // Estilizando o mapa;
  // Criando um array com os estilos
  var styles = [
    {
      stylers: [
        { hue: "#ccc" },
        { saturation: 5 },
        { lightness: -10 },
        { gamma: 1.51 }
      ]
    },
    {
      featureType: "road",
      elementType: "geometry",
      stylers: [
        { lightness: 100 },
        { visibility: "simplified" }
      ]
    },
    {
      featureType: "road",
      elementType: "labels"
    }
  ];

  // crio um objeto passando o array de estilos (styles) e definindo um nome para ele;
  var styledMap = new google.maps.StyledMapType(styles, {
    name: "Mapa Style"
  });

  // Aplicando as configurações do mapa
  map.mapTypes.set('map_style', styledMap);
  map.setMapTypeId('map_style');

}


// Função para carregamento assíncrono
function loadScript() {
  var script = document.createElement("script");
  script.type = "text/javascript";
  script.src = "http://maps.googleapis.com/maps/api/js?key=AIzaSyDmjkEri5pEh98mYRj8Jq9b4XqK3LoU_6w&sensor=true&callback=initialize";
  document.body.appendChild(script);
}

window.onload = loadScript;
