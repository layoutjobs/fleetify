﻿<?php error_reporting(0); ?>
<!doctype html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <title>::: FLEETIFY - MONITORAMENTO DE FROTA :::</title>
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet">
        <link rel="stylesheet" href="assets/css/main.css">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->


    <script>
        function formatar(mascara, documento){
          var i = documento.value.length;
          var saida = mascara.substring(0,1);
          var texto = mascara.substring(i)
          
          if (texto.substring(0,1) != saida){
                    documento.value += texto.substring(0,1);
          }
          
        }
    </script>


    </head>

<body data-spy="scroll" data-target=".navbar" data-offset="50" style="overflow-x: hidden;">

    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#" data-smooth-scroll><img src="assets/images/logo.png"><span class="frase-menu">SUA MELHOR IMAGEM A BORDO</span></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#apresentacao" data-smooth-scroll>Apresentação</a></li>
            <li><a href="#diferenciais" data-smooth-scroll>Diferenciais</a></li>
            <li><a href="#beneficios" data-smooth-scroll>Benefícios</a></li>
            <li><a href="#tecnologia" data-smooth-scroll>Equipamentos</a></li>
            <li><a href="#contato" data-smooth-scroll>Fale Conosco</a></li>
            <li>
            <img class="icon-phone" src="assets/images/icon-phone.png"></li> <span class="phone">55 (19) 3500.4860<br>55 (19) 3500.4859</span>
            </li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
                            
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <section class="block-banner" role="banner">
        <div class="container">
            <h1><img src="assets/images/marca-banner.png"></h1>
            <button class="btn"><a class="btn-saiba-mais" href="#apresentacao" data-smooth-scroll>SAIBA MAIS</a></button>
            <div class="arrow-down">
                <a href="#apresentacao" data-smooth-scroll><img src="assets/images/arrow-down.png"></a>
            </div>
        </div>
    </section>

    <section class="block block-apresentacao" id="apresentacao">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>MONITORAMENTO<br> DE FROTA</h2>
                    <p>
                        O Fleetify é o serviço que monitora sua frota baseado nos eventos e imagens de alta definição que a REI oferece através de sua solução de Vídeo Monitoramento. Utilizando uma rede Wi-Fi e 3G/4G, o Fleetify proporciona acesso ágil às informações dos veículos, como localização, trajeto, gravação de imagens, vídeos em tempo real, relatórios e status. Fleetify é um serviço completo, inovador e totalmente integrado com os equipamentos da REI Brasil. 
                    </p>
                    <p><img src="assets/images/logo-amarelo.png"></p>
                </div>
                <div class="col-md-6"><img src="assets/images/computador.png"></div>
            </div>
        </div>
    </section>


    <div class="bg-amarelo">
        <div class="container-fluid">
            <div class="col-md-6">
                <img class="img-onibus" src="assets/images/img-onibus.png">
            </div>
            <div class="container">
                <div class="col-md-6">
                    <div class="content">
                        <span class="before"></span><p>Fleetify é uma solução integrada que reduz 
                        o fluxo de trabalho, organiza os dados, fornece 
                        a melhor imagem e todas as informações 
                        que você precisa para monitorar sua frota.</p><span class="after"></span>
                    </div>
                </div>    
            </div>
        </div>
    </div>

    <section class="block block-diferenciais" id="diferenciais">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                <img src="assets/images/logo-rei-diferenciais.png" width="275" height="71">
                    <h2>DIFERENCIAIS</h2>
                        <p>* Plataforma de Serviço 100% na Nuvem</p>
                        <p>* Download automático de eventos e vídeos Wi-Fi e <strong>3G/4G</strong>.</p>
                        <p>* Diversas formas de análise (dashboard, listas, mapas e relatórios)</p>
                        <p>* Visualização real time da localização e eventos dos veículos</p>
                        <p>* Visualização real time das imagens dos veículos</p>
                        <p>* A gestão de eventos pode ser customizada de acordo com as suas necessidades</p>
                        <p>* Os eventos são configuráveis por sensores e localização</p>
                        <p><strong>Todas as informações são transmitidas via</strong> <img class="icon-wifi" src="assets/images/icon-wifi.png"></p>
                    
                </div>
                <div class="col-md-6"><img src="assets/images/cell.png" width="663" height="410"></div>
            </div>
        </div>

    </section>

    <section class="block-servico">
        <img src="assets/images/marca-banner.png">
        <p>A melhor imagem para a segurança dos passageiros, do condutor, do veículo e da empresa. </p>
    </section>

    <section class="block block-beneficios" id="beneficios">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img src="assets/images/logo-rei-diferenciais.png">
                    <h2>BENEFÍCIOS</h2>
                    <p>* Imagens e alertas em tempo real</p>
                    <p>* Combater fraudes causadas pelos funcionários</p>
                    <p>* Combater assaltos, furtos e agressões</p>
                    <p>* Combater evasão de receitas</p>
                    <p>* Atenuação de riscos jurídicos</p>
                    <p>* Elucidação de acidentes</p>
                    <p>* Controle da depredação interna dos veículos</p>
                    <p> * Combater o mal comportamento dos usuários</p>
                    <p><strong>Imagens e alertas em real time.</strong></p>
                </div>
                <div class="col-md-6">
                    <img src="assets/images/compurador-dashboard.png">
                </div>
                <div class="container">
                    <figure><img class="img-produtos" src="assets/images/img-produtos.png"></figure>
                    <p>O sistema Fleetify trabalha 100% online. Desta forma todos os dados são captados e transferidos para armazenamento em nuvem em tempo real, facilitando e agilizando o acesso de todo monitoramento em qualquer localidade. Todas informações são transferidas via Wi-Fi, e 3G/4G.</p>
                    <p>A REI Brasil fornece todo o suporte em pessoal e tecnologia para o monitoramento da sua frota.</p>
                    <p>A REI oferece a solução completa de Vídeo Monitoramento. Além do Fleetify, a REI proporciona todo o conjunto de equipamentos de qualidade internacionalmente reconhecidos.</p>
                    <p>Os DVRs da série HD5 gravam as imagens em alta definição através de câmeras de alta resolução estrategicamente posicionadas. O modelo HD5-600 permite até 4 câmeras AHD, e 2 câmeras IP. O modelo HD5-1200 permite ligar até 8 câmeras AHD, e 4 câmeras IP.  Os acessos as imagens ocorrem através de um disco rígido removível de 1TB, download das gravações via Wi-Fi, e acesso on-line via 3G e/ou 4G. O DVR HD5 da REI é um produto homologado pela ANATEL, que registra vídeos, localização e eventos que ocorreram com o veículo, possibilitando sua análise.</p>
                    <p>Somos uma empresa em conformidade com os requisitos da norma ISO 9001:2015</p>
                    <p>Produtos com garantia de 1 ano.</p>
                    <p>Nossos produtos são homologados pelas principais encarroçadoras do Brasil.</p>
                </div>
    </section>


    <section class="block-banner-suporte">
        <p>A REI Brasil também oferece todo o suporte em pessoal e tecnologia para o monitoramento da sua frota. </p>
    </section>

                
    <section class="block block-implantacao">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <h2>IMPLANTAÇÃO</h2>
                        <p>
                            A REI oferece todo o suporte técnico para a instalação do Fleetify.<br>
                            Desde a instalação e treinamento dos usuários, como também todo o<br>
                            apoio técnico para a instalação de todos os equipamentos. 
                        </p>
                </div>
                <div class="col-md-4">
                    <span class="arrow"></span>
                        <p class="content"><strong>Apoio técnico</strong></p>
                        <p class="content">Apoio na implantação</p>
                        <p class="content">Suporte especializado</p>
                        <p class="content">Apoio aos gestores</p>
                        <br>
                    <span class="arrow02"></span>
                        <p class="content"><strong>Apoio estratégico</strong></p>
                        <p class="content">Apoio na elaboração de relatórios</p>
                        <p class="content">Treinamento</p>
                        <p class="content">Acompanhamento</p> 
                </div>
            </div>
        </div>                
    </section>



  <div class="bg-amarelo-download">
        <div class="container-fluid">
            <div class="container">
                <div class="col-md-6">
                    <div class="content">
                        <p>
                            A REI oferece a solução completa para Vídeo Monitoramento. Além do Fleetify, a REI proporciona todo o conjunto de equipamentos com a melhor tecnologia do mercado nacional.<br><br>
                            <a class="download-pdf" href="fleetify.pdf" target="_blank">Faça download PDF Fleetify</a>
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                <span class="img-onibus-download"></span>   
                </div> 
            </div>       
            
        </div>
    </div>

    <section class="block block-tecnologia" id="tecnologia">
        <div class="container">
            <div class="row">         
                <div class="col-md-6">
                    <img class="logo-rei-tec" src="assets/images/logo-rei-diferenciais.png">
                </div>
                <div class="col-md-6 pull-right"> 
                    <img src="assets/images/selo-dnv.jpg">
                </div>

                <div class="col-md-12">
                    <h2>ALTA TECNOLOGIA</h2>
                    <p>Além dos diferenciais do sistema Fleetify, a REI oferece todos os equipamentos para a máxima qualidade em Vídeo Monitoramento. Os DVRs da série HD5 gravam as imagens em alta definição através de câmeras de alta resolução estrategicamente posicionadas. O modelo HD5-600 permite ligar até 4 câmeras AHD, e 2 câmeras IP. O modelo HD5-1200 permite ligar até 8 câmeras AHD, e 4 câmeras IP.  Os acessos as imagens ocorrem através de um disco rígido removível de 1TB, download das gravações via Wi-Fi e acesso on-line via 3G e/ou 4G. Registra vídeos, localização e eventos que ocorreram com o veículo, possibilitando sua análise.</p>

                    <p><strong>Somos uma empresa em conformidade com os requisitos da norma ISSO 9001:2015. Produtos com garantia de 1 ano.</strong></p>
                    <p><strong>Nossos produtos são homologados pela Anatel e pelas principais encarroçadoras do Brasil.</strong></p>
                    
                    <figure>
                    <img src="assets/images/cameras-acessorios.png">
                    <img src="assets/images/acessorios.png">
                    </figure>
                    <p class="text-center">Acesse todos os modelos no site da REI Brasil.</p>
                </div>
            </div>
        </div>

    </section>

    <section class="block block-fale-conosco" id="contato">
        <div class="container">
           <div class="row">
                <div class="col-md-12">
                    <h2>FALE CONOSCO</h2> 
                    <p>Seja qual for a solução que você precisa em imagem, a REI tem os equipamentos e soluções desenvolvidos com base em tecnologia embarcada, perfeitos para sua fábrica ou empresa de ônibus. De vídeo-monitoramento a entretenimento, você tem a melhor imagem para garantir a eficiência da sua operação e da sua empresa.</p>

                    <p class="content">Conheça mais sobre a REI Brasil, acesse: <strong>www.reibrasil.com.br</strong></p>
                </div>
                <div class="col-md-9 pull-right">
                    <form id="contact-form" action="" method="POST">
                        <div class="form-group">
                            <label for="nome">NOME</label>
                            <input type="text" class="form-control" name="nome" required>
                            <label for="email">EMAIL</label>
                            <input type="email" class="form-control" name="email" required>
                            <label for="whatsapp">WHATSAPP</label>
                            <input type="text" class="form-control" name="telefone" maxlength="13" placeholder="(DDD) 99999-9999" OnKeyPress="formatar('##-#####-####', this)" required>
                            <label for="duvida">DÚVIDAS/INFO</label>
                            <input type="text" class="form-control" name="msg" required>
                              <div id="resp"></div>
                        </div>
                        <button type="submit" class="btn pull-right" value="ENVIAR">ENVIAR</button>
                      
                    </form>
                    </div>
            </div>

            <div class="row">
                    <div class="col-md-9">
                        <a class="telefone" href="tel:551935004860">55 (19) 3500.4860</a><br>
                        <a class="telefone" href="tel:551935004859">55 (19) 3500.4859</a>
                        <div class="email">
                        <a class="email" href="mailto:contato@reibrasil.com.br">contato@reibrasil.com.br</a>
                        </div>
                    </div>
            </div>
                    <address>
                        REI BRASIL - Rod. Engenheiro Ermenio de Oliveira Penteado, KM 57,7 Indaiatuba/SP
                    </address>
        </div> 

                <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>
        
    </section>
      
    <div id="mapa"></div>

<script src="assets/js/main.js"></script>
<script src="contato.js"></script>
</body>
</html>