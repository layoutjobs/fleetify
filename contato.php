﻿<?php
// Inclui o arquivo class.phpmailer.php localizado na pasta class
//require_once("phpmailer/class.phpmailer.php");
require_once 'phpmailer/PHPMailerAutoload.php';
//header('Content-Type: application/json');
 
 // variáveis que guardam os dados vindo do form
$nomeusuario = !empty($_POST['nome']) ? $_POST['nome'] : '';
$emailusuario = !empty($_POST['email']) ? $_POST['email'] : die('Por favor, preencha o seu e-mail no formulário de contato.');
$telefone = !empty($_POST['telefone']) ? $_POST['telefone'] : die('Por favor, preencha o telefone no formulário de contato.');	
$mensagem = !empty($_POST['msg']) ? $_POST['msg'] : die('Por favor, preencha a mensagem no formulário de contato.');	
// Inicia a classe PHPMailer
$mail = new PHPMailer(true);
 
// Define os dados do servidor e tipo de conexão
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
$mail->IsSMTP(); // Define que a mensagem será SMTP
 
try {
     $mail->Host = 'smtp.gmail.com'; // Endereço do servidor SMTP (Autenticação, utilize o host smtp.seudomínio.com.br)
     $mail->SMTPSecure = 'tls';
     $mail->SMTPAuth   = true;  // Usar autenticação SMTP (obrigatório para smtp.seudomínio.com.br)
     $mail->Port       = 587; //  Usar 587 porta SMTP
     $mail->Username = 'contatofleetify@gmail.com'; // Usuário do servidor SMTP (endereço de email)
     $mail->Password = 'radioindustries@'; // Senha do servidor SMTP (senha do email usado)

 
     //Define o remetente
     // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=    
     $mail->SetFrom('contatofleetify@gmail.com', 'REI Brasil'); //Seu e-mail
     $mail->AddReplyTo($emailusuario, $nomeusuario); //Seu e-mail
     $mail->Subject = 'Hotsite Fleetifly > Contato';//Assunto do e-mail
 
 
     //Define os destinatário(s)
     //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
     $mail->AddAddress('contatofleetify@gmail.com');
 
     //Campos abaixo são opcionais 
     //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
     //$mail->AddCC('destinarario@dominio.com.br', 'Destinatario'); // Copia
     //$mail->AddBCC('destinatario_oculto@dominio.com.br', 'Destinatario2`'); // Cópia Oculta
     //$mail->AddAttachment('images/phpmailer.gif');      // Adicionar um anexo
			

     //Define o corpo do email
	$mail->MsgHTML("
		<html>
		<head>
		</head>
		<table>
		<tr><td><p><strong>Nome:</strong> ".$nomeusuario."</p></td></tr>
		<tr><td><p><strong>E-mail:</strong> ".$emailusuario."</p></td></tr>
		<tr><td><p><strong>Telefone:</strong> ".$telefone."</p></td></tr>
		<tr><td><p><strong>Mensagem:</strong> ".$mensagem."</p></td></tr>
        </table>
		</html>
	"); 
 
     ////Caso queira colocar o conteudo de um arquivo utilize o método abaixo ao invés da mensagem no corpo do e-mail.
     //$mail->MsgHTML(file_get_contents('arquivo.html'));
 
     $mail->Send();
     echo "Mensagem enviada com sucesso. Obrigado pelo contato.\n";
 
    //caso apresente algum erro é apresentado abaixo com essa exceção.
    }catch (phpmailerException $e) {
      echo $e->errorMessage(); //Mensagem de erro costumizada do PHPMailer
}

?>